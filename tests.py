#===============================================
#   IMPORTS
#===============================================

import chromatogram as chrom



#===============================================
#   GLOBALS
#===============================================

file_path_00 = "data-src/main_data.txt"
file_path_01 = "data-src/sars-cov-2.txt"


#===============================================
#   MAIN
#===============================================

if (__name__ == "__main__"):
    chrom = chrom.GraphData()       # creating main object


    converted_data = chrom.convert_data( file_path_01, "r" )

    chrom.display_graph(converted_data, True, 150)



#===============================================
#   END OF FILE
#===============================================