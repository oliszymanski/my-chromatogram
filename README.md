# My-Chromatogram

### Proper description
Personal chromatogram for one of my existing projects'. Hope I will get
this thing to work properly as it should


The chromatogram is used to show how many nucleotides are there in the
whole genome. All of it is displayed on a plot. I will use `matplotlib`
to generate the chromatogram on a different canvas.


<br>

## sample images
It all goes to displaying everything on a plot from `matplotlib` and here are
a few samples that I have tested myself:

In this image, I have used data which is stored in `data-src` directory of a
filename `main_data.txt`:
<img src="test_data_plots/data_plot_00.png" >


<br>

This image represents a genome of the `Sars-CoV-2` virus which is in the same
directory as the example above. Part of the genome stored in `sars-cov-2.txt` file:

<img src="test_data_plots/data_plot_01.png">

<br>

## Data source
Data source is in a `readme.md` file in <a href="https://gitlab.com/oliszymanski/Count-DNA-Nucleotides">
this</a> project